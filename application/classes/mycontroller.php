<?php defined('SYSPATH') or die('No direct script access.');

class Mycontroller extends Controller_Template {

    public function before(){
        return parent::before();
    }
    public function after(){
        Breadcrumbs::generate_from_request($this->request);
        $this->template->crumbs = Breadcrumbs::render();
        
        $conf = Kohana::$config->load('titles');
        if(!isset($this->template->title)){
            if(key_exists($this->request->controller(), $conf)){
                $this->template->title = $conf[$this->request->controller()]['title'].' - Make me Warm';
                $this->template->desc = $conf[$this->request->controller()]['desc'].' - Make me Warm';
            }else{
                $this->template->title = 'Make me Warm - hand made магазин | вязані та деревяні речі, іграшки, аксесуари | купити, замовити';
                $this->template->desc = 'Make me Warm - hand made магазин | вязані та деревяні речі, іграшки, аксесуари | купити, замовити';
            }
        }
        return parent::after();
    }
    
} 