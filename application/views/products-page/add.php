<section class="content">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <? if(isset($success)){ ?>
                <!-- Success box -->
                <div class="box box-solid box-success">
                    <div class="box-header">
                        <h3 class="box-title">Operation succeeded!</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;">
                        <? if(is_object($success)){?>
                            <p><a href="/product/show/<?= $success->id; ?>"><?= $success->title; ?></a> was successfully changed.</p>
                        <?}else{?>
                            <p><?= $success; ?></p>
                        <?}?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            <?}else if(isset($errors)){?>
                <!-- Danger box -->
                <div class="box box-solid box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Operation failed!</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-danger btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-danger btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <? foreach($errors as $e){?>
                            <p><?=$e;?></p>
                        <?}?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            <?}?>
        </div>
    </div>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add new product: </h3>                                    
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form action="/products/add" method="post" class="form" id="add-product">
                        <p>
                            <label>Title
                                <input type="text" name="title" class="form-control">
                            </label>
                        </p>
                        <p>
                            <label>Content
                                <textarea name="content" class="form-control" rows="3" placeholder="Enter content here..."></textarea>
                            </label>
                        </p>
                        <p>
                            <label>Price
                                <input type="number" name="price" class="form-control">
                            </label>
                        </p>
                        <p>
                            <label>Quantity
                                <input type="number" name="quantity" class="form-control">
                            </label>
                        </p>
                        <p>
                            <label>Needed Quantity
                                <input type="number" name="needed_quantity" class="form-control">
                            </label>
                        </p>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
<link   type="text/css" href="/css/datatables/dataTables.bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/js/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript"> $(function(){$('#example1').dataTable();}); </script>