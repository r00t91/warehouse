<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Products extends Mycontroller {
    
    public $template = 'base';
    
    public function action_index(){
        $data['header'] = 'Hello, products-page';
        $this->template->content = View::factory('products-page/base', $data);
    }

    public function action_search(){
        $product            = new Model_Product();
        $data['search_key'] = $this->request->post('search_key');
        $data['products']   = $product->search($data['search_key']);
        $this->template->content = View::factory('products-page/base', $data);
    }
    
    public function action_change_quantity(){
        $product            = new Model_Product();
        if(!$product->change_quantity($this->request->param('id'), $this->request->post('quantity'))){
            $data['errors'] = $product->getErrors(); 
        }else{
            $data['success'] = ORM::factory('Product', $this->request->param('id')); 
        }
        $data['search_key'] = $this->request->post('search_key');
        $data['products']   = $product->search($data['search_key']);
        $this->template->content = View::factory('products-page/base', $data);
    }
    
    public function action_change_needed_quantity(){
        $product            = new Model_Product();
        if(!$product->change_needed_quantity($this->request->param('id'), $this->request->post('quantity'))){
            $data['errors'] = $product->getErrors(); 
        }else{
            $data['success'] = ORM::factory('Product', $this->request->param('id')); 
        }
        $data['search_key'] = $this->request->post('search_key');
        $data['products']   = $product->search($data['search_key']);
        $this->template->content = View::factory('products-page/base', $data);
    }
    
    public function action_delete(){
        if(ORM::factory('Product', $this->request->param('id'))->delete()){
            $data['success'] = "Successfully deleted."; 
        }else{
            $data['errors'] = "There is some problems while deleting this product. Try again later or contact admin."; 
        }
        $product            = new Model_Product();
        $data['search_key'] = $this->request->post('search_key');
        $data['products']   = $product->search($data['search_key']);
        $this->template->content = View::factory('products-page/base', $data);
    }
    public function action_add(){
        $data = array();
        if($this->request->post()){
            $product    = new Model_Product();
            $new        = ORM::factory('Product', $product->addProduct($this->request->post()));
            if($new->loaded()){
                $data['success'] = $new; 
            }else{
                $data['errors'] = $product->getErrors(); 
            }
        }
        $this->template->content = View::factory('products-page/add', $data);
    }
} // End Products
