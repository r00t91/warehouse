<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Mycontroller {
    
    public $template = 'base';
    
    public function action_index(){
        $this->template->content = View::factory('home-page/base');
    }

} // End Home
