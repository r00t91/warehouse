<?php defined('SYSPATH') or die('No direct script access.');

class Mycontroller extends Controller_Template {

    public function before(){
        return parent::before();
    }
    public function after(){
        $conf = Kohana::$config->load('titles');
        if(!isset($this->template->title)){
            if(key_exists($this->request->controller(), $conf)){
                $this->template->title = $conf[$this->request->controller()]['title'].' - WAREHOUSE';
                $this->template->desc = $conf[$this->request->controller()]['desc'].' - WAREHOUSE';
            }else{
                $this->template->title = 'WAREHOUSE';
                $this->template->desc = 'WAREHOUSE';
            }
        }
        return parent::after();
    }
    
} 