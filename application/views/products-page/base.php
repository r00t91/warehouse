<section class="content">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <? if(isset($success)){ ?>
                <!-- Success box -->
                <div class="box box-solid box-success">
                    <div class="box-header">
                        <h3 class="box-title">Operation succeeded!</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body" style="display: block;">
                        <? if(is_object($success)){?>
                            <p><a href="/product/show/<?= $success->id; ?>"><?= $success->title; ?></a> was successfully changed.</p>
                        <?}else{?>
                            <p><?= $success; ?></p>
                        <?}?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            <?}else if(isset($errors)){?>
                <!-- Danger box -->
                <div class="box box-solid box-danger">
                    <div class="box-header">
                        <h3 class="box-title">Operation failed!</h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-danger btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button class="btn btn-danger btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <p><?=$errors; ?></p>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            <?}?>
        </div>
    </div>
</section>
<div class="box">
    <div class="box-header">
        <h3 class="box-title">Search results for "<?= $search_key; ?>": </h3>                                    
    </div><!-- /.box-header -->
    <?//= Kohana_Debug::vars($products);?>
    <div class="box-body table-responsive">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Q-ty</th>
                    <th>Q-ty needed</th>
                    <th>Q-ty diff</th>
                    <th>Price</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <? foreach($products as $p){ 
                    $sum = $p->quantity - $p->quantity_needed; 
                    if($sum < 0){$bg = 'red';}else if($sum < 10){$bg = 'yellow';}else{$bg = 'green';}?>
                    <tr>
                        <td><?= $p->id;?></td>
                        <td><a href="/product/show/<?= $p->id;?>"><?= $p->title;?></a></td>
                        <td>
                            <form action="/products/change_quantity/<?= $p->id;?>" method="post" class="sidebar-form">
                                <div class="input-group">
                                    <input type="number" name="quantity" class="form-control" value="<?= $p->quantity;?>">
                                    <span class="input-group-btn">
                                        <button type="submit" name="prod_search_btn" id="search-btn" class="btn btn-flat"><i class="fa fa-check-square"></i></button>
                                    </span>
                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="/products/change_needed_quantity/<?= $p->id;?>" method="post" class="sidebar-form">
                                <div class="input-group">
                                    <input type="number" name="quantity" class="form-control" value="<?= $p->quantity_needed;?>">
                                    <span class="input-group-btn">
                                        <button type="submit" name="prod_search_btn" id="search-btn" class="btn btn-flat"><i class="fa fa-check-square"></i></button>
                                    </span>
                                </div>
                            </form>
                        </td>
                        <td class="bg-<?=$bg;?>"><?= $sum;?></td>
                        <td><?= $p->coast;?></td>
                        <td>
                            <div class="tools">
                                <a href="/products/delete/<?= $p->id;?>">
                                    <i class="fa fa-trash-o"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?}?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Q-ty</th>
                    <th>Q-ty needed</th>
                    <th>Q-ty diff</th>
                    <th>Price</th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<link   type="text/css" href="/css/datatables/dataTables.bootstrap.css" rel="stylesheet"/>
<script type="text/javascript" src="/js/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="/js/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript"> $(function(){$('#example1').dataTable();}); </script>