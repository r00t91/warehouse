<?php defined('SYSPATH') or die('No direct script access.');

class Model_Product extends ORM {

    protected $tableName;
    protected $nstree;
    protected $errors = array();
    protected $_created_column = array('column' => 'ctime', 'format' => TRUE);
    
    public function labels(){
        return array(
            'title'             => 'Title',
            'content'           => 'Content',
            'coast'             => 'Price',
            'quantity'          => 'Quantity',
            'quantity_needed'   => 'Quantity needed'
        );
    }
    
    public function rules(){
        return array(
            'title' => array(
                array('not_empty'),
                array('min_length', array(':value', 3)),
                array('max_length', array(':value', 32)),
                array(array($this, 'unique'), array('title', ':value')),
            ),
            'content' => array(
                array('not_empty'),
                array('min_length', array(':value', 5)),
                array('max_length', array(':value', 1024)),
            ),
            'coast' => array(
                array('not_empty'),
                array('digit'),
                array('min_length', array(':value', 1)),
                array('max_length', array(':value', 7)),
                array('range', array(':value', 0, 999999)),
            ),
            'quantity' => array(
                array('not_empty'),
                array('digit'),
                array('min_length', array(':value', 1)),
                array('max_length', array(':value', 10)),
            ),
            'quantity_needed' => array(
                array('not_empty'),
                array('digit'),
                array('min_length', array(':value', 1)),
                array('max_length', array(':value', 10)),
            )
        );
    }

    public function search($search_key) {
        return is_int($search_key) ? 
            ORM::factory('Product')->where('id', '=', $search_key)->find_all() 
                : 
            ORM::factory('Product')->where('title', 'like', "%$search_key%")->find_all();
    }
    
    public function change_quantity($id, $quantity) {
        $prod = ORM::factory('Product', $id);
        $prod->quantity = $quantity;
        try{
            $prod->save();
            return TRUE;
        }catch(ORM_Validation_Exception $e){
            $this->errors = $e->errors();
            return FALSE;
        }
    }
    
    public function change_needed_quantity($id, $quantity) {
        $prod = ORM::factory('Product', $id);
        $prod->quantity_needed = $quantity;
        try{
            $prod->save();
            return TRUE;
        }catch(ORM_Validation_Exception $e){
            $this->errors = $e->errors();
            return FALSE;
        }
    }

    public function addProduct($data) {
        try{
            $prod = ORM::factory('Product');
            $prod->quantity_needed  = $data['needed_quantity'];
            $prod->quantity         = $data['quantity'];
            $prod->content          = $data['content'];
            $prod->title            = $data['title'];
            $prod->coast            = $data['price'];
            $prod->save();
            return TRUE;
        }catch(ORM_Validation_Exception $e){
            $this->errors = $e->errors('models');
            return FALSE;
        }
    }

    public function getErrors() {
        return $this->errors;
    }

}
